function mnist_generate_new_categories_data

matfile = 'mnist_npz.mat'; %rename from badly named "mnist_test.mat"
n_training_digits = 7; %if 7 training, then 3 left out for test
n_per_combo = 5000;
img_dim = 28; %should always be true in mnist
outname_format = 'ptc_mnist_newcat_train_%d%d%d%d%d%d%d_test_%d%d%d.mat';

rng('shuffle')
dat = load(matfile);

train_data = dat.x_train; % 60000 x imgdim1 x imgdim2
train_labels = dat.y_train; % 1 x 60000

train_sets = combnk(1:10, n_training_digits); %really 0-9 but easier to work with in Matlab as 1-10
digit_inds = cell(10,1);
digit_ns   = nan(10,1);
for i = 0:9
    digit_inds{i+1} = find(train_labels==i);
    digit_ns(i+1) = numel(digit_inds{i+1});
end

for i = 1:size(train_sets,1)
    fprintf('Set %d of %d\n',i,size(train_sets,1));
    this_train_set = train_sets(i,:);
    this_test_set = setdiff(1:10,this_train_set);
    
    train_diff_combos = combnk(this_train_set,2);
    train_diff_combos = [train_diff_combos; fliplr(train_diff_combos)]; %#ok<AGROW>
    n_train_diff_combos = size(train_diff_combos,1);
    n_diffs_total = n_train_diff_combos * n_per_combo;
    
    diff_dat = uint8(zeros(n_train_diff_combos,n_per_combo,2,img_dim,img_dim));
    for j = 1:n_train_diff_combos
        fprintf('- diff combo %d of %d\n',j,n_train_diff_combos);
        dig1_inds = digit_inds{train_diff_combos(j,1)};
        dig2_inds = digit_inds{train_diff_combos(j,2)};
        
        [tmp1, tmp2] = meshgrid(uint16(1:numel(dig1_inds)),uint16(1:numel(dig2_inds)));
        possible_diff_inds = [tmp1(:) tmp2(:)]; %a little thorny b/c these are really indices of indices
        n_possible_diff_inds = size(possible_diff_inds,1);
        possible_diff_inds_rand_inds = randperm(n_possible_diff_inds, n_per_combo);
        
        diff_inds_to_use = possible_diff_inds(possible_diff_inds_rand_inds,:);
        
        dig1_inds_to_use = dig1_inds(diff_inds_to_use(:,1));
        dig2_inds_to_use = dig2_inds(diff_inds_to_use(:,2));
        diff_data1 = train_data(dig1_inds_to_use,:,:);
        diff_data2 = train_data(dig2_inds_to_use,:,:);
        
        diff_dat(j,:,1,:,:) = diff_data1;
        diff_dat(j,:,2,:,:) = diff_data2;
    end
        
    n_sames_per_digit = n_diffs_total / n_training_digits;
    same_dat = uint8(zeros(n_training_digits,n_sames_per_digit,2,img_dim,img_dim));
    for j = 1:n_training_digits
        fprintf('- same digit %d of %d\n',j,n_training_digits);
        this_same_dig = this_train_set(j);
        same_dig_inds = digit_inds{this_same_dig};
        
%         possible_same_inds = combnk( uint16(numel(same_dig_inds)), 2 );
%         possible_same_inds = [possible_same_inds; fliplr(possible_same_inds)]; %#ok<AGROW>
%         n_possible_same_inds = size(possible_same_inds,1);
%         possible_same_inds = possible_same_inds(randperm(n_possible_same_inds),:);
%         
%         same_inds_to_use = possible_same_inds(1:n_sames_per_digit);
        
        [tmp1, tmp2] = meshgrid(uint16(1:numel(same_dig_inds)),uint16(1:numel(same_dig_inds)));
        possible_same_inds = [tmp1(:) tmp2(:)]; %a little thorny b/c these are really indices of indices
        n_possible_same_inds = size(possible_same_inds,1);
        possible_same_inds_rand_inds = randperm(n_possible_same_inds, n_sames_per_digit + 1000); %the 1000 is a bit of a hack... part A of this hack here
        
        same_inds_to_use = possible_same_inds(possible_same_inds_rand_inds,:);
        same_inds_to_use(same_inds_to_use(:,1) == same_inds_to_use(:,2), :) = []; %part B of the hack
        same_inds_to_use = same_inds_to_use(1:n_sames_per_digit,:); %part C of the hack, finit
        
        dig1_inds_to_use = same_dig_inds(same_inds_to_use(:,1));
        dig2_inds_to_use = same_dig_inds(same_inds_to_use(:,2));
        same_data1 = train_data(dig1_inds_to_use,:,:);
        same_data2 = train_data(dig2_inds_to_use,:,:);
        
        same_dat(j,:,1,:,:) = same_data1;
        same_dat(j,:,2,:,:) = same_data2;
    end
    
    n_test_examples_base = sum(digit_ns(this_test_set));
    test_dat = uint8(zeros(n_test_examples_base,10,2,2,img_dim,img_dim)); %~18000 x digits x top/bottom x img1/img2 x pixels x pixels
    all_test_inds = [digit_inds{this_test_set}];
    for k = 1:10
        fprintf('- test digit %d of 10\n',k);
        k_inds = digit_inds{k};
        n_k_inds = digit_ns(k);
        
        for j = 1:n_test_examples_base
            this_j_ind = all_test_inds(j);
            other_ind1 = this_j_ind;
            while other_ind1 == this_j_ind
                other_ind1 = k_inds(randi(n_k_inds));
            end
            
            other_ind2 = this_j_ind;
            while other_ind2 == this_j_ind || other_ind1 == other_ind2
                other_ind2 = k_inds(randi(n_k_inds));
            end
            
            test_dat(j,k,1,1,:,:) = train_data(this_j_ind,:,:);
            test_dat(j,k,1,2,:,:) = train_data(other_ind1,:,:);
            
            test_dat(j,k,2,2,:,:) = train_data(this_j_ind,:,:);
            test_dat(j,k,2,1,:,:) = train_data(other_ind2,:,:);
        end
    end
    
    %need to save out some other metadata too, like what all the digits are, but let's see if this works first
    
    diff_dat = reshape(diff_dat,[n_train_diff_combos*n_per_combo,2,img_dim,img_dim]);
    same_dat = reshape(same_dat,[n_training_digits*n_sames_per_digit,2,img_dim,img_dim]);
    training_digits_data = [same_dat; diff_dat];
    training_digits_labels = zeros(size(training_digits_data,1),1);
    training_digits_labels(1:size(same_dat,1)) = 1; %same
    training_digits_labels(training_digits_labels==0) = 2; %#ok<NASGU> %diff
    
    test_dat = reshape(test_dat,[n_test_examples_base*10*2,2,img_dim,img_dim]); %#ok<NASGU>
    
    output_digits_tmp = num2cell([sort(this_train_set-1) sort(this_test_set-1)]);
    output_fname = sprintf(outname_format,output_digits_tmp{:});
    save(output_fname, 'training_digits_data','training_digits_labels','test_dat');
end
    

%general notes
% 60000 digits in training set
% so 6000 per digit
% if we take 7/10 digits for training that's 42000 examples
% if each gets one "same" and one "diff", that's 84000
% that may be overkill trying to balance categories
% instead, say we combnk it? (think that's right, double check)
% that's 36 million possible pairings for each pair of digits
% there are 42 digit-pair combinations
% let's say we take a random 5,000 of each -- that's 210,000 "diff"
% and add the same number of "same" -- total 420,000. nice.


% for test, we have 6000 x 3 digits = 18000 trials
% - maybe for each one we just pick one random other trial from each category
% - which gives 180,000 test trials
% - or two? once top, once bottom? for 360,000 test trials