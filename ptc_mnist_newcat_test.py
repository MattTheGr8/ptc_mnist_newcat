import keras
import scipy.io

model_file  = 'fill_me_in_later'
data_file   = 'me_too_boss'
output_file = 'whatever'

trained_model = keras.models.load_model(model_file)
matdata = scipy.io.loadmat(data_file)
test_dat = matdata['test_dat']
preds = trained_model.predict(test_dat)
scipy.io.savemat(output_file, preds)
